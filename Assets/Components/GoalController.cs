﻿using UnityEngine;
using System.Collections;

public class GoalController : MonoBehaviour {

	public float startHeight;
	public float hoverSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 floatVector = this.transform.up * Mathf.Cos (Time.time * hoverSpeed) * 0.5f;
		this.transform.position = new Vector3 (this.transform.position.x, floatVector.y + startHeight, this.transform.position.z);
	}
}
