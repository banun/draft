﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuController : MonoBehaviour {

	private int leftDeviceIndex;
	private int rightDeviceIndex;

	private void Initialize () {
//		controller = GetComponent<SteamVR_TrackedController>();
		leftDeviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost);
		rightDeviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost);
	}

// Use this for initialization
	void Start () {
		Initialize ();	
	}
	
	// Update is called once per frame
	void Update () {
		if (SteamVR_Controller.Input(rightDeviceIndex).GetPress(SteamVR_Controller.ButtonMask.Grip) || SteamVR_Controller.Input(leftDeviceIndex).GetPress(SteamVR_Controller.ButtonMask.Grip)) {
			LevelManager.SharedInstance.ResetToFirstLevel();
			SceneManager.LoadScene("Play");
		}
	}

	void Reset () {
		Initialize ();
	}
}
