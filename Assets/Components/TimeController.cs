﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(SteamVR_TrackedController))]
//[RequireComponent(typeof(SteamVR_TrackedController))]

public class TimeController : MonoBehaviour {

//	SteamVR_TrackedController controller;
	public float timeScaleRate;
	private int leftDeviceIndex;
	private int rightDeviceIndex;

	private void Initialize () {
//		controller = GetComponent<SteamVR_TrackedController>();
		leftDeviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost);
		rightDeviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost);
	}
	// Use this for initialization
	void Start () {
		Initialize ();
	}

	// Update is called once per frame
	void Update () {
		Debug.Log("Left:" + leftDeviceIndex + " Right:" + rightDeviceIndex);

		float amountOfTimeThatPassedThisFrame = Time.unscaledDeltaTime;
		float dTimeScale = timeScaleRate * amountOfTimeThatPassedThisFrame;
		if (SteamVR_Controller.Input(rightDeviceIndex).GetPress(SteamVR_Controller.ButtonMask.Grip) || SteamVR_Controller.Input(leftDeviceIndex).GetPress(SteamVR_Controller.ButtonMask.Grip)) {
			dTimeScale *= -1;
		}
		Time.timeScale = Mathf.Clamp01(Time.timeScale + dTimeScale);	
	}

	void Reset() {
		Initialize ();
//		controller.SetDeviceIndex(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost));
	}
}
